package umkhaya.co.za.bookscannerapp.Utils.Models;

/**
 * Created by C315AM on 2018/04/03.
 */

public class SurveyRequest
{
    private int okay;

    private String IMEI;

    private String ISBN;

    private int good;

    private int bad;

    public int getOkay ()
    {
        return okay;
    }

    public void setOkay (int okay)
    {
        this.okay = okay;
    }

    public String getIMEI ()
    {
        return IMEI;
    }

    public void setIMEI (String IMEI)
    {
        this.IMEI = IMEI;
    }

    public String getISBN ()
    {
        return ISBN;
    }

    public void setISBN (String ISBN)
    {
        this.ISBN = ISBN;
    }

    public int getGood ()
    {
        return good;
    }

    public void setGood (int good)
    {
        this.good = good;
    }

    public int getBad ()
    {
        return bad;
    }

    public void setBad (int bad)
    {
        this.bad = bad;
    }

}