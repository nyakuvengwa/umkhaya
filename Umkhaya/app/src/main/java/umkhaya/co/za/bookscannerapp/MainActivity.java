package umkhaya.co.za.bookscannerapp;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityCompat.OnRequestPermissionsResultCallback;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import umkhaya.co.za.bookscannerapp.Utils.Models.SurveyRequest;
import umkhaya.co.za.bookscannerapp.Utils.Models.SurveyResponse;
import umkhaya.co.za.bookscannerapp.Utils.UtilHelper;
import umkhaya.co.za.bookscannerapp.Utils.WebHelpers.WebController;

public class MainActivity extends AppCompatActivity implements Callback<SurveyResponse>, OnRequestPermissionsResultCallback {
    private static final int PERMISSIONS_REQUEST_READ_PHONE_STATE = 999;

    static ProgressDialog progressDialog;
    private ConstraintLayout constraintLayout;
    private EditText ed;
    private static final int RC_BARCODE_CAPTURE = 9001;
    private static final String TAG = "BarcodeMain";
    private NumberPicker good_numberPicker;
    private NumberPicker okay_numberPicker;
    private NumberPicker bad_numberPicker;
    private TelephonyManager telephonyManager;
    private String IMEI_Number_Holder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //  dialog = (ProgressDialog) findViewById(R.id.main_progressBar);

        constraintLayout = (ConstraintLayout) findViewById(R.id.main_wrapper);
        ed = (EditText) findViewById(R.id.isbn_edit_text);

        FloatingActionButton scanner_button = (FloatingActionButton) findViewById(R.id.btn_scanner);
        scanner_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), BarcodeCaptureActivity.class);
                intent.putExtra(BarcodeCaptureActivity.AutoFocus, true);

                startActivityForResult(intent, RC_BARCODE_CAPTURE);
            }
        });
        good_numberPicker = (NumberPicker) findViewById(R.id.good_numberPicker);
        good_numberPicker.setMinValue(0);
        good_numberPicker.setMaxValue(100);
        good_numberPicker.setWrapSelectorWheel(true);

        okay_numberPicker = (NumberPicker) findViewById(R.id.okay_numberPicker);
        okay_numberPicker.setMinValue(0);
        okay_numberPicker.setMaxValue(100);
        okay_numberPicker.setWrapSelectorWheel(true);


        bad_numberPicker = (NumberPicker) findViewById(R.id.bad_numberPicker);
        bad_numberPicker.setMinValue(0);
        bad_numberPicker.setMaxValue(100);
        bad_numberPicker.setWrapSelectorWheel(true);

        good_numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int oldValue, int newValue) {
                //  ed.setText("Selected Good Num :" + newValue);
            }
        });

        okay_numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int oldValue, int newValue) {
                //   ed.setText("Selected okay Num :" + newValue);
            }
        });

        bad_numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int oldValue, int newValue) {
                //    ed.setText("Selected bad Num :" + newValue);
            }
        });

        FloatingActionButton submitBtn = (FloatingActionButton) findViewById(R.id.submit_btn);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SubmitSurvey();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_BARCODE_CAPTURE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
                    try {
                        if (ed == null) {
                            ed = (EditText) findViewById(R.id.isbn_edit_text);
                        }
                        ed.setText(barcode.displayValue);
                    } catch (Exception ex) {
                        Log.e(TAG, getString(R.string.barcode_error, ex));
                    }
                    Log.d(TAG, "Barcode read: " + barcode.displayValue);
                } else {
                    Snackbar.make(constraintLayout, R.string.barcode_failure, 10).show();
                    Log.d(TAG, "No barcode captured, intent data is null");
                }
            } else {
                Snackbar.make(constraintLayout, R.string.barcode_failure, 10).show();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onResponse(Call<SurveyResponse> call, Response<SurveyResponse> response) {

        if (response.isSuccessful()) {
            SurveyResponse surveyResponse = response.body();
            if (surveyResponse != null) {
                ed.setText(null);
                good_numberPicker.setDisplayedValues(null);
                okay_numberPicker.setDisplayedValues(null);
                bad_numberPicker.setDisplayedValues(null);
                progressDialog.dismiss();
                //TODO: show success
                Toast.makeText(this, R.string.successful_record,
                        Toast.LENGTH_LONG).show();
            }
        } else {
            progressDialog.dismiss();
            Toast.makeText(this, R.string.server_error,
                    Toast.LENGTH_LONG).show();
            System.out.println(response.errorBody());
            Log.d(TAG, "onResponse: " + response.errorBody());
        }
    }

    @Override
    public void onFailure(Call<SurveyResponse> call, Throwable t) {
        progressDialog.dismiss();
        Toast.makeText(this, R.string.server_error,
                Toast.LENGTH_LONG).show();
        Log.d(TAG, "No connectivity");
    }

    void SubmitSurvey() {
        String ISBN = null;
        int goodValue = 0;
        int badValue = 0;
        int okayValue = 0;


        try {

            ISBN = ed.getText().toString();
            okayValue = okay_numberPicker.getValue();
            badValue = bad_numberPicker.getValue();
            goodValue = good_numberPicker.getValue();
        } catch (Exception ex) {
            Log.e(TAG, "SubmitSurvey: " + ex.getMessage(), ex);
        }
        final SurveyRequest request = new SurveyRequest();
        request.setBad(badValue);
        request.setGood(goodValue);
        request.setOkay(okayValue);
        request.setISBN(ISBN);
        GetIMEI();
        if (IMEI_Number_Holder!=null) {
            request.setIMEI(IMEI_Number_Holder);
        }
        else
        {
            GetIMEI();
            request.setIMEI(IMEI_Number_Holder);
        }

//        Objects inventoryObjects = new Objects();
//        inventoryObjects.set__type("Pointer");
//        inventoryObjects.setClassName("Inventory");
//        inventoryObjects.setCode(ISBN);
//        Objects[] bookObjects = new Objects[]{inventoryObjects};
//        Book book = new Book();
//        book.setObjects(bookObjects);
//        book.set__op("AddRelation");
//
//        //User Objects
//        umkhaya.co.za.bookscannerapp.Utils.Objects userObjects = new umkhaya.co.za.bookscannerapp.Utils.Objects();
//        userObjects.set__type("Pointer");
//        userObjects.setClassName("_User");
//        userObjects.setObjectId("BUG_Testing_App");
//        umkhaya.co.za.bookscannerapp.Utils.Objects[] usersObjects = new umkhaya.co.za.bookscannerapp.Utils.Objects[]{userObjects};
//        User user = new User();
//        user.set__op("AddRelation");
//        user.setObjects(usersObjects);
//
//
//        Survey survey = new Survey();
//        survey.setBook(book);
//        survey.setUser(user);
//        survey.setBad(badValue);
//        survey.setGood(goodValue);
//        survey.setOkay(okayValue);

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage("You are about to submit " + ed.getText().toString() + " book report. Are you sure?")
                .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        if (progressDialog == null) {
                            progressDialog = UtilHelper.CreateProgressDialog(MainActivity.this);
                            progressDialog.show();
                        } else {
                            progressDialog.show();
                        }
                        try {
                            WebController controller = new WebController(getString(R.string.base_url_webservices));
                            controller.SubmitSurvey(request, MainActivity.this);
                        } catch (Exception ex) {
                            Log.e(TAG, "SubmitSurvey: " + ex.getMessage(), ex);
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void GetIMEI() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
try {
    if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSIONS_REQUEST_READ_PHONE_STATE);
        TelephonyManager telephonyManager;
        telephonyManager = (TelephonyManager) getSystemService(Context.
                TELEPHONY_SERVICE);
        IMEI_Number_Holder = telephonyManager.getDeviceId();
    } else {
        //TODO
        TelephonyManager telephonyManager;
        telephonyManager = (TelephonyManager) getSystemService(Context.
                TELEPHONY_SERVICE);
        IMEI_Number_Holder = telephonyManager.getDeviceId();
    }

}catch (Exception ex)
{
    Log.e(TAG, "GetIMEI: "+ex.getMessage(), ex);
    Toast.makeText(this ,"Permissions Denied. Closing App." , Toast.LENGTH_LONG);
    android.os.Process.killProcess(android.os.Process.myPid());
    System.exit(1);
}

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        // BEGIN_INCLUDE(onRequestPermissionsResult)
        if (requestCode == PERMISSIONS_REQUEST_READ_PHONE_STATE) {
            // Request for camera permission.
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission has been granted. Start camera preview Activity.

               GetIMEI();
            } else {
                // Permission request was denied.
                Toast.makeText(this ,"Permissions Denied. Closing App." , Toast.LENGTH_LONG);
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
            }
        }
        // END_INCLUDE(onRequestPermissionsResult)
    }

}
