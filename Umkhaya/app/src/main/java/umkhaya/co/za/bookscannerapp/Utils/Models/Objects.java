package umkhaya.co.za.bookscannerapp.Utils.Models;

/**
 * Created by C315AM on 2018/03/30.
 */

public class Objects
{
    private String __type;

    private String className;

    private String code;

    public String get__type ()
    {
        return __type;
    }

    public void set__type (String __type)
    {
        this.__type = __type;
    }

    public String getClassName ()
    {
        return className;
    }

    public void setClassName (String className)
    {
        this.className = className;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }
}
