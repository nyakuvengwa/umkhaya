package umkhaya.co.za.bookscannerapp;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

public class SplashActivity extends AppCompatActivity {

    private static final String TAG = "SplashActivity";

    private int flag = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        TextView appVersionTextView = (TextView) findViewById(R.id.txt_splash_version);

        String versionName = getString(R.string.app_version_dummy);
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            versionName = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            Log.d(TAG, "onCreate: " + e.getMessage());
            e.printStackTrace();
        }
        appVersionTextView.setText("Version: " + versionName);
        Thread thread = new Thread() {
            public void run() {
                try {
                    while (flag != 4) {
                        Log.d("Splash", "flag " + flag);
                        sleep(1000);
                        flag++;
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    Log.d(TAG, "flag " + e.getMessage());
                } finally {

                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        };

        thread.start();
    }

}

