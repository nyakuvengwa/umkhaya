package umkhaya.co.za.bookscannerapp.Utils.Models;

/**
 * Created by C315AM on 2018/03/30.
 */

public class Survey {
    private int okay;

    private Book book;

    private int good;

    private int bad;

    private User user;

    public int getOkay ()
    {
        return okay;
    }

    public void setOkay (int okay)
    {
        this.okay = okay;
    }

    public Book getBook ()
    {
        return book;
    }

    public void setBook (Book book)
    {
        this.book = book;
    }

    public int getGood ()
    {
        return good;
    }

    public void setGood (int good)
    {
        this.good = good;
    }

    public int getBad ()
    {
        return bad;
    }

    public void setBad (int bad)
    {
        this.bad = bad;
    }

    public User getUser ()
    {
        return user;
    }

    public void setUser (User user)
    {
        this.user = user;
    }
}
