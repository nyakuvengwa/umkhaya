package umkhaya.co.za.bookscannerapp.Utils.WebHelpers;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import umkhaya.co.za.bookscannerapp.Utils.Models.SurveyRequest;
import umkhaya.co.za.bookscannerapp.Utils.Models.SurveyResponse;

/**
 * Created by C315AM on 2018/03/30.
 */

public interface WebApi {
    @Headers("X-Parse-Application-Id: umkhaya_app_id")
    @POST("/parse/classes/Survey") // staging production
    Call<SurveyResponse> SubmitSurvey(@Body SurveyRequest survey);

}
