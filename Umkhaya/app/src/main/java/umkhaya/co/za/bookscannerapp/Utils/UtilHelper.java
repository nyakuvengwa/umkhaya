package umkhaya.co.za.bookscannerapp.Utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;

import umkhaya.co.za.bookscannerapp.R;

/**
 * Created by C315AM on 2018/03/30.
 */

public class UtilHelper {
    String TAG = "UtilHelper";

    public static ProgressDialog CreateProgressDialog(Context mContext) {
        ProgressDialog dialog = new ProgressDialog(mContext);
        try {
            dialog.show();
        } catch (Exception e) {

        }
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_progress_bar);
        return dialog;
    }
}
