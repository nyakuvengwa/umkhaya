package umkhaya.co.za.bookscannerapp;

import android.app.Application;

/**
 * Created by C315AM on 2018/03/30.
 */

public class MyApp extends Application {
    private static MyApp instance;
    public static MyApp getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        instance = this;
        super.onCreate();
    }

}
