package umkhaya.co.za.bookscannerapp.Utils.Models;

/**
 * Created by C315AM on 2018/03/30.
 */
public class SurveyResponse
{
    private String createdAt;

    private String objectId;

    public String getCreatedAt ()
    {
        return createdAt;
    }

    public void setCreatedAt (String createdAt)
    {
        this.createdAt = createdAt;
    }

    public String getObjectId ()
    {
        return objectId;
    }

    public void setObjectId (String objectId)
    {
        this.objectId = objectId;
    }
}