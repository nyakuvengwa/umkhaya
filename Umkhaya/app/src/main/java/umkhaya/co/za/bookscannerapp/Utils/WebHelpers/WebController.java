package umkhaya.co.za.bookscannerapp.Utils.WebHelpers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import umkhaya.co.za.bookscannerapp.Utils.Models.SurveyRequest;
import umkhaya.co.za.bookscannerapp.Utils.Models.SurveyResponse;

/**
 * Created by C315AM on 2018/03/30.
 */

public class WebController {
    Gson gson;
    OkHttpClient okHttpClient;
    Retrofit retrofit;

    public WebController(String baseUrl) {
        final String BASE_URL;BASE_URL = baseUrl;
        gson = new GsonBuilder()
                .setLenient()
                .create();

        okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    public  void  SubmitSurvey (SurveyRequest survey , Callback<SurveyResponse> callback)
    {
        WebApi webApi = retrofit.create(WebApi.class);
        Call<SurveyResponse> surveyResposnseCall = webApi.SubmitSurvey(survey);
        surveyResposnseCall.enqueue(callback);

    }
}