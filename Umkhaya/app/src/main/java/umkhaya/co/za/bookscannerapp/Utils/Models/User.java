package umkhaya.co.za.bookscannerapp.Utils.Models;

import umkhaya.co.za.bookscannerapp.Utils.Objects;

/**
 * Created by C315AM on 2018/03/30.
 */

public class User
{
private umkhaya.co.za.bookscannerapp.Utils.Objects[] objects;
private String __op;

    public Objects[] getObjects ()
    {
        return objects;
    }

    public void setObjects (Objects[] objects)
    {
        this.objects = objects;
    }

    public String get__op ()
    {
        return __op;
    }

    public void set__op (String __op)
    {
        this.__op = __op;
    }

}

